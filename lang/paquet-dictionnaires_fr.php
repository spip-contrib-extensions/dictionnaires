<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/dictionnaires.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dictionnaires_description' => 'Permet la création de dictionnaires regroupant des définitions. Les termes définis sont automatiquement détectés dans les textes du site et il est alors possible de les insérer dans le HTML du texte de manière accessible.',
	'dictionnaires_nom' => 'Dictionnaires',
	'dictionnaires_slogan' => 'Créer des dictionnaires'
);
